///////////////////////////////////////////////////////////////////////////////
//          University of Hawaii, College of Engineering
/// @brief  Lab 06c - countdown - EE 205 - Spr 2022
//
// Usage:  countdown
//
// Result:
//   Counts upwards from the day I was born
//
// Example:
// Reference time: Thu Feb 04 17:25:06 PM HST 1999
// Years: 23  Days: 17  Hours: 9  Minutes: 7  Seconds: 52
// Years: 23  Days: 17  Hours: 9  Minutes: 7  Seconds: 53
// Years: 23  Days: 17  Hours: 9  Minutes: 7  Seconds: 54
// Years: 23  Days: 17  Hours: 9  Minutes: 7  Seconds: 55
// Years: 23  Days: 17  Hours: 9  Minutes: 7  Seconds: 56
// Years: 23  Days: 17  Hours: 9  Minutes: 7  Seconds: 57
// Years: 23  Days: 17  Hours: 9  Minutes: 7  Seconds: 58
// Years: 23  Days: 17  Hours: 9  Minutes: 7  Seconds: 59
//
// @author Mariko Galton <mgalton@hawaii.edu>
// @date   23_Feb_2022
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <time.h>
#include <stdbool.h>
#include <unistd.h>

int main() {
   
   char target_reference_time[80];
   struct tm target;
   struct tm *current_time;
   time_t target_time;
   time_t local_time;

      target.tm_sec = 6;             // range from 0 to 61 seconds after a minute
      target.tm_min = 25;             // range from 0 to 59 minutes after the hour
      target.tm_hour = 17;            // range from 0 to 23 hours since midnight 
      target.tm_mday = 4;            // range from 1 to 31 days of the month
      target.tm_mon = 2-1;             // range from 0 to 11 months since January
      target.tm_year = 1999-1900;            // years since 1900
      

      //target.tm_wday;            // range from 0 to 6 days since Sunday
      //target.tm_yday;            // range from 0 to 365 days since January 1
      target.tm_isdst = -1;           // Daylight Saving Time flag

      target_time = mktime(&target);
      strftime (target_reference_time,80, "%a %b %d %X %p %Z %Y", &target);

      printf("Reference time: %s \n", target_reference_time);    

   while (true) {
      time(&local_time);
      current_time = localtime(&local_time);
      local_time = time(NULL);
  
      // int diff_year = ((local_time - target_time)/3.154e7); 
      // int diff_day = ((local_time - target_time)/86400);
       int diff_hour =(((local_time - target_time)/3600)%24); 
       int diff_min =(((local_time - target_time)/60)%60);
       int diff_sec =((local_time - target_time)%60);
       
       printf("Years: %d  Days: %d  Hours: %d  Minutes: %d  Seconds: %d  \n", current_time->tm_year-target.tm_year, current_time->tm_mday-target.tm_mday, diff_hour, diff_min, diff_sec);
  
       sleep(1);
   }
   
   return 0;
}
